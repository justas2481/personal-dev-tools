// Tools to work with things that require Bash knowledge.
// Just for a bit of automation when it's needed.
package main

import (
	"flag"
	"fmt"
	"os"
	"personal-dev-tools/phpstorm"
)

const (
	programVersion = "personal-dev-tools V1.1"
)

var versionFlag = flag.Bool(
	"version",
	false,
	"Current version of a program.",
)

var extsFlag = flag.String(
	"exts",
	phpstorm.DefaultExts,
	"Extensions to be allowed when sending files to PHPStorm formatter.",
)

func main() {
	flag.Parse()
	args := flag.Args()
	if *versionFlag {
		fmt.Println(programVersion)
		return
	}

	if len(args) < 1 {
		fmt.Fprintln(
			os.Stderr,
			"No commands provided. use \"help\" command to find out available options.",
		)
		os.Exit(1)
	}

	applyCMD(args[0])
}

// Apply a particular command.
func applyCMD(CMD string) {
	var err error
	switch CMD {
	case "phpstorm-format-staged":
		err = phpstorm.FormatStaged(*extsFlag)
	case "help":
		showHelp()
	default:
		fmt.Fprintln(
			os.Stderr,
			"There is no such command.",
		)
		os.Exit(2)
	}

	if err != nil {
		fmt.Fprintln(
			os.Stderr,
			err,
		)
	}
}

func showHelp() {
	fmt.Println(
		"To format staged files in your project, use \"phpstorm-format-staged\" command.",
	)
	fmt.Println()
	fmt.Println(
		"To pass wanted extensions, use \"--exts\" flag before command and separate them by ; (semi).",
	)
	fmt.Println(
		"For example: \"--exts=\".php;.css\" phpstorm-format-staged\"",
	)
	fmt.Println(
		"If you do not pass \"--exts\" flag, it will automatically format these files:",
		phpstorm.DefaultExts,
	)
	fmt.Println()
	fmt.Println(
		"If you want to add particular standard rules file for the PHPStorm formatter, set environment variable PERSONAL-DEV-TOOLS-PHPSTORM-STANDARD")
	fmt.Println(
		"and pass a string value as path to the standard.",
	)
	fmt.Println()
	fmt.Println(
		"To check the program version, use --version flag.",
	)
}
