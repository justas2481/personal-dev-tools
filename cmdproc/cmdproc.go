// Deals with commands which are sent to OS.
package cmdproc

import "os/exec"

// Executes command
func Execute(cmd string, args []string) (out string, err error) {
	output, err := exec.Command(cmd, args...).Output()
	if err != nil {
		return "", err
	}

	out = string(output)
	return
}
