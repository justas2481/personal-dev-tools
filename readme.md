# Personal Dev Tools

This project was born quickly due to the need of formatting PHP files by using Visual Studio Code and keeping PHPStorm's formatting convension. Since this convension might be in a separate standards file and might be incompatible with newer PHPCS (Code Sniffer), this app lets us to automate process quite a bit.

Under the hood, it invokes PHPStorm's shell script which allows to format files acording to given rools. To insure that you can use Personal Dev Tools to invoke PHPStorm's formatter, follow this link and insure you can execute given commands without errors: [https://www.jetbrains.com/help/phpstorm/command-line-formatter.html](https://www.jetbrains.com/help/phpstorm/command-line-formatter.html).

This project is named so that we may add new independent features as we go which are totaly unrelated to PHPStorm.

## Features

1. Automatically format staged files of your current branch with the help of PHPStorm.
1. Decide which file extensions should be formatted.
1. If it's necessary, set a path to the standards file which is recognised by PHPStorm.
1. You can skip particular files (if need to do so not by extension, but by the concrete file), just by avoiding to stage them before formatting your files that are ready to be commited and do that afterwards.

## Requirements

* PHPStorm installed and activated
* Linux platform (tested on Ubuntu only)
* Git installed
* Go tools to be able to compile app from a source code. You can find Go here: [https://go.dev/](https://go.dev/)

## Setup

* Clone this repo.
* Compile this app from a source code.
* Move compiled app to the bin directory, for example:

  **$ sudo mv /path/to/personal-dev-tools  /usr/local/bin/personal-dev-tools**
* Add phpstorm.sh path to your $PATH environment variable and make sure that phpstorm.sh has execute permission. If it does not, add it like so:

  **$ sudo chmod +x path-to-file/phpstorm.sh**

  Add execute permission to personal-dev-tools as well if needed.

  [There is an explanation](https://unix.stackexchange.com/questions/26047/how-to-correctly-add-a-path-to-path) if you don't know how to add a new path to your $PATH environment variable.
* Check if app can be loaded by issuing:

  **$ personal-dev-tools --version**
* Move to the root directory of the project you're working in.
* Make sure you have initialized a Git repository, because this app uses Git to find out which files are staged and where to find them.
* Use **help** command to find out how to use this app in detail.