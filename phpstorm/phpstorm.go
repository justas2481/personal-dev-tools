// Apply commands for a PHPStorm to be able to format certain cases.
package phpstorm

import (
	"fmt"
	"os"
	"path/filepath"
	"personal-dev-tools/cmdproc"
	"strings"
)

const (
	// Separate exts by
	ExtsSep = ";"
	// Wich extensions to pass to formatter by default
	DefaultExts = ".php" + ExtsSep + ".tpl" + ExtsSep + ".js"
	// How many files to pass for formatting to PHPStorm per chunk
	fFilesPerChunk = 10
)

// Determines which files are staged and sends them to PHPStorm formatter.
func FormatStaged(allowedExts string) error {
	// Check if there is a project root
	cwd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf(
			"can not retrieve working dir.\nmore info: %v",
			err,
		)
	}

	// Root dir of a project must have .git dir
	fileinfo, err := os.Stat(cwd + "/.git")
	if os.IsNotExist(err) {
		fmt.Println(
			"project root has no initialized git repository",
		)
		return nil
	}
	if err != nil {
		return fmt.Errorf(
			"can not obtain info about the project root\nmore info: %v",
			err,
		)
	}
	if fileinfo.Mode().IsRegular() {
		fmt.Println(
			"For some reason you have file called .git instead of a directory,",
			"which represents your source history.",
			"Please check what's wrong happened with a project.",
		)
		return nil
	}

	// Obtain staged files
	var cmd = "git"
	var args = []string{"diff", "--name-only", "--cached"}
	out, err := cmdproc.Execute(cmd, args)
	if err != nil {
		return err
	}
	if len(out) == 0 {
		fmt.Println(
			"There are no staged files yet...",
		)
		return nil
	}
	stagedFiles := strings.Split(out, "\n")
	if len(stagedFiles) < 1 {
		fmt.Println(
			"Can not retrieve staged files...",
		)
		return nil
	}

	// Check if PHPStorm's script is in $PATH
	cmd = "phpstorm.sh"
	envPath := os.Getenv("PATH")
	if envPath == "" {
		fmt.Println(
			"Your $PATH is empty...",
		)
		return nil
	}
	paths := strings.Split(envPath, ":")
	if len(paths) < 1 {
		fmt.Println(
			"Can not obtain environment path separate elements.",
		)
		return nil
	}
	if !findFileInPaths(paths, cmd) {
		fmt.Println(
			"Can not find phpstorm.sh file in any $PATH.",
			"Please move phpstorm.sh script to the one of a $PATH paths.",
			"They are as follows:",
		)
		fmt.Println(
			envPath,
		)
		return nil
	}

	// Determine if external standards file is attached
	externalStandard := os.Getenv("PERSONAL-DEV-TOOLS-PHPSTORM-STANDARD")
	externalStandardLen := len(externalStandard)
	if externalStandardLen == 0 {
		fmt.Println(
			"Format will be invoked without external standards file...",
		)
	} else if !fileExists(externalStandard) {
		fmt.Println(
			"Can not locate PHPStorm standards file by following your ENV variable.",
		)
		return nil
	}
	args = []string{"format", ""}
	if externalStandardLen > 0 { // Standards file attached
		args[len(args)-1] = "-s"
		args = append(args, externalStandard, "")
	}
	argsLen := len(args)

	// Determine allowed extensions
	exts := strings.Split(allowedExts, ExtsSep)
	allowAllExts := len(exts) == 1 && exts[0] == ""

	fmt.Println(
		"Formatting files:",
	)

	// Format staged files
	var files strings.Builder
	var i int
	for _, filename := range stagedFiles {
		// Skip empty lines
		if filename == "" {
			continue
		}
		// Add quotes if filename has spaces
		if strings.Contains(filename, " ") {
			filename = "\"" + filename + "\""
		}
		// Format only those files which are in allowed extensions
		if !allowAllExts && !isAllowedExt(exts, filepath.Ext(filename)) {
			fmt.Println(
				"[Skipping]",
				filename,
			)
			continue
		}
		fmt.Println(
			filename,
		)
		// Filename is not empty, so write to the buffer with the directory prefix
		files.WriteString(cwd + "/" + filename + " ")
		// Mark that current file is added
		i++
		// Pass only certain amount of files
		if (i+1)%fFilesPerChunk == 0 {
			// Add retrieved files as an argument
			args[argsLen-1] = files.String()
			// Invoke PHPStorm formatter
			if _, err := cmdproc.Execute(cmd, args); err != nil {
				return fmt.Errorf(
					"for some reason formatter was not invoked or threw an error: %v",
					err,
				)
			}
			files.Reset()
			i = 0
		}
	}

	fmt.Println(
		"Format complete.",
	)

	return nil
}

// Checks if file by it's name exists in a given paths.
func findFileInPaths(paths []string, filename string) bool {
	for _, path := range paths {
		if fileExists(path + "/" + filename) {
			return true
		}
	}

	return false
}

// Checks if file exists following the given path.
func fileExists(path string) bool {
	if fileinfo, err :=
		os.Stat(path); err == nil && fileinfo.Mode().IsRegular() {
		return true
	}

	return false
}

// Determines whether given extension is within allowed extensions slice.
func isAllowedExt(exts []string, ext string) bool {
	for _, allowedExt := range exts {
		if ext == allowedExt {
			return true
		}
	}

	return false
}
